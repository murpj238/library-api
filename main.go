package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	_ "github.com/mattn/go-sqlite3"
	. "log"
	"net/http"
)

type
(
	BookModel struct {
		gorm.Model
		ISBN string `json:"isbn"`
		Author string `json:"author"`
		Title string `json:"title"`
		PublishDate string `json:"publish_date"`
		Publisher string `json:"publisher"`
		Edition string `json:"edition"`
		Binding string `json:"binding"`
	}
)
var db *gorm.DB
func init() {
	//open a db connection
	var err error
	db, err = gorm.Open("sqlite3", "/tmp/library.db")
	if err != nil {
		panic("failed to connect database")
	}
	//Migrate the schema
	db.AutoMigrate(&BookModel{})
}

func addBook(c *gin.Context) {
	book := BookModel{ ISBN: c.PostForm("isbn"), Author: c.PostForm("author"), Title: c.PostForm("title"), PublishDate: c.PostForm("publish_date"), Publisher: c.PostForm("publisher"), Edition: c.PostForm("edition"), Binding: c.PostForm("binding")}
	db.NewRecord(book)
	result := db.Create(&book)
	if result.Error != nil {
		c.JSON(http.StatusForbidden,gin.H{"status": http.StatusForbidden, "message": "Failed to add new book!", "resourceId": book.ISBN})
	}
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Book successfully added!", "resourceId": book.ISBN})
}

func getLibrary(c *gin.Context)  {
	var books []BookModel

	db.Find(&books)

	if len(books) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No books found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": books})
}

func getBook(c *gin.Context) {
	var book BookModel

	db.First(&book, c.Param("isbn"))

	if len(book.ISBN) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No book found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": book})
}

func updateLibrary(c *gin.Context)  {
	var book BookModel

	db.First(&book, c.Param("isbn"))

	if len(book.ISBN) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No book found!"})
		return
	}

	updates := db.Model(&book).Updates(
		BookModel{
			Author:      c.PostForm("author"),
			Title:       c.PostForm("title"),
			PublishDate: c.PostForm("published_date"),
			Publisher:   c.PostForm("publisher"),
			Edition:     c.PostForm("edition"),
			Binding:     c.PostForm("binding"),
		})
	if updates.Error != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"status": http.StatusNotAcceptable, "message": "Book failed to update!"})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Book updated successfully!"})
}

func removeBook(c *gin.Context)  {
	var book BookModel

	db.First(&book,c.Param("isbn"))
	if len(book.ISBN) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No book found!"})
		return
	}
	db.Delete(&book)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Book deleted successfully!"})
}

func main() {
	router := gin.Default()

	v1 := router.Group("/api/v1/library")
	{
		v1.POST("/",addBook)
		v1.GET("/",getLibrary)
		v1.GET("/:isbn",getBook)
		v1.PUT("/:isbn",updateLibrary)
		v1.DELETE("/:isbn",removeBook)
	}

	Fatal(router.Run())
}