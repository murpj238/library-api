FROM golang:1.12.7 as builder

WORKDIR /usr/build

COPY main.go .

RUN go get -d ./...

RUN go build -o app .

EXPOSE 8080

CMD ["./app"]