# Library API

## Code Description

This is a small RESTful service developed with Go.

The service allows CRUD operations on a collection of books in a library.

The REST API is implemented using the GIN HTTP web framework.

I went with the ISBN as a unique identifier over a separate ID field as the isbn is already a unique to the book.

However, the database model automatically creates fields on the database. So each book also has an ID which can be used for search.

Four methods have been created for this:

* Get Single: Retrieves a single book from the SqlLite database using the isbn.

```
    e.g. GET http://localhost:8080/api/v1/library/978-3-16-148410-0
    e.g. GET http://localhost:8080/api/v1/library/1
```

* Get All: Retrieve all books from teh SqlLite database.

```
    e.g. GET http://localhost:8080/api/v1/library
```
    
* Create: Adds a book to the SqlLite database.

```
    e.g. POST http://localhost:8080/api/v1/library
       {
           "isbn":"978-3-16-148410-0",
           "author":"Jack Murphy",
           "title":"Jack Murphy's Brilliant Book",
           "publish_date":"20190801",
           "publisher":"Dev Books",
           "Edition":"First Edition",
           "Binding":"Paperback"
       }
```

    
* Update: Updates any of the properties of the books based using the isbn.

```
    e.g. PUT http://localhost:8080/api/v1/library/978-3-16-148410-0
       {
           "title":"Jack Murphy's Even Better Book",
           "publish_date":"20190808"
       }
```

    
* Delete: Deletes a book using the isbn.

```
    e.g. DELETE http://localhost:8080/api/v1/library?isbn=978-3-16-148410-0
```
    
## Automated Build

I've used GitLab CI to build the Dockerfile and push it to my Gitlab private repository.

The Dockerfile simply takes the go file, pulls down the packages and builds the application.

## Running the Container

```
sudo docker pull registry.gitlab.com/murpj238/library-api:<LAST COMMIT ID>
sudo docker run -i -p 8080:8080 registry.gitlab.com/murpj238/library-api:<LAST COMMIT ID>
```

## Improvements to be Made

* Use multi-stage build in the Dockerfile

* Add deployment stage to the CI file to deploy to Kubernetes cluster

* Add friendly UI for the REST API 